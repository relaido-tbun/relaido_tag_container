import '@babel/polyfill';
import jQuery from 'jquery';
const $ = jQuery;

let apiURl = "";
// 開発環境
if (process.env.NODE_ENV !== 'production') {
  apiURl = "http://ad-api-v01-jp-dev.ulizaex.com/work/han/api/rtmdata.php"
  console.log('development mode!');
} else {
  apiURl = "http://ad-api-v01-jp-dev.ulizaex.com/work/han/api/rtmdata.php"
  console.log('production mode!');
}
let tagContainer = null;
let tagContainerID = "";
// タグコンテナID取得
tagContainer = $("#RELAIDO-TG");
try {
  if (tagContainer.length > 0) {
    tagContainerID = tagContainer.data("id")
    if (tagContainerID) {
      // タグマネージャー情報収集
      $.post(apiURl,
      {
          tagContainerID: tagContainerID,
      },
      function(data, status){
        tagCreate(data)
      }, "json")
      .fail(function() {
         throw new Error( "get the tag manager data failed" );
       });
    } else {
      throw new Error("no tag container id")
    }
  } else {
    throw new Error("no script")
  }
} catch (e) {
  console.log(e.message);
}

async function tagCreate(rtmdata) {
  let rtmId = rtmdata.rtm_id;
  let rtmTags = rtmdata.tags;
  rtmTags.forEach(async rtdata => {
    await new Promise(resolve => {
      $(document).ready(function(){
        let adTarget = null;
        // IDターゲット要素
        if (rtdata.rtd_target_type == 1) {
          adTarget = document.getElementById(rtdata.rtd_target);
        } else if ((rtdata.rtd_target_type == 2)){
          // CSSセレクタターゲット要素
          adTarget = $(rtdata.rtd_target)[0];
        } else {
          adTarget = document.body;
        }

        if (rtdata.rtd_type == 1) {
          appendChildScript(adTarget, rtdata.rtd_src)
        } else {
          $(adTarget).append(rtdata.rtd_html);
        }
      });
    });
  });
}
//
function appendChildScript(target, src) {
  let script = document.createElement('script');
  script.src = src;
  target.appendChild(script);
}

const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
 entry: {
      rtm: ["@babel/polyfill",'./src/rtm.js'],
      "0.1.0/rtg": ["@babel/polyfill",'./src/rtg.js']
 },
 plugins: [
   new CleanWebpackPlugin(['dist']),
   new HtmlWebpackPlugin({
     title: 'Relaido TAG MANAGER Production'
   }), // Generates default index.html
   new HtmlWebpackPlugin({
     title: 'Relaido TAG MANAGER TEST',
     filename: 'test.html',
     template: 'src/assets/index.html',
     inject: false
   })
 ],
 output: {
   filename: '[name].js',
   path: path.resolve(__dirname, 'dist')
 },
 module: {
   rules: [
     {
       // 拡張子 .js の場合
       test: /\.(js|jsx)$/,
       use: [
         {
           // Babel を利用する
           loader: 'babel-loader',
           // Babel のオプションを指定する
           options: {
             presets: [
               // プリセットを指定することで、ES2018 を ES5 に変換
               '@babel/preset-env',
             ]
           }
         }
       ]
     }
   ]
 }
};
